1. Describe possible performance optimizations for your Code?
Step check can be done using middlewares. But here because of short time it was done in controller.
If I run the api call using a queue system in the background, it will be better than executing http request inside another. The id then could be saved and shown in another place than registration success page.

2. Which things could be done better, than you�ve done it?
If the states are saved using ajax requests or Vuejs axios, it will be much more user friendly.
Also if I used a framework like laravel, it would be much better.
