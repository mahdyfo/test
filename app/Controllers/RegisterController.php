<?php

namespace App\Controllers;

use App\Helpers\ViewHelper;
use App\Models\Address;
use App\Models\BankAccount;
use App\Models\User;

class RegisterController
{
    const PERSONAL_STEP = 1;
    const ADDRESS_STEP = 2;
    const BANK_STEP = 3;
    /**
     * We check for last user data to redirect him to appropriate page
     */
    public function showLastSaved()
    {
        if (!isset($_SESSION['step'])) {
            header('Location: register/step' . self::PERSONAL_STEP);
            exit;
        }

        if ($_SESSION['step'] === 4) {
            exit('Registration was finished');
        }

        header('Location: register/step' . (int)$_SESSION['step']);
        exit;
    }

    public function showPersonalForm()
    {
        // Step check
        if (isset($_SESSION['step']) && $_SESSION['step'] > self::PERSONAL_STEP) {
            header('Location: step' . self::ADDRESS_STEP);
            exit;
        }

        echo (new ViewHelper())->view('register/PersonalForm');
    }

    public function showAddressForm()
    {
        // Step check
        if (!isset($_SESSION['step'])) {
            header('Location: step' . self::PERSONAL_STEP);
            exit;
        }
        if ($_SESSION['step'] > self::ADDRESS_STEP) {
            header('Location: step' . self::BANK_STEP);
            exit;
        }

        echo (new ViewHelper())->view('register/AddressForm');
    }

    public function showPaymentForm()
    {
        // Step check
        if (!isset($_SESSION['step'])) {
            header('Location: step' . self::PERSONAL_STEP);
            exit;
        }
        if ($_SESSION['step'] > self::BANK_STEP) {
            header('Location: success');
            exit;
        }

        echo (new ViewHelper())->view('register/BankAccountForm');
    }

    public function storePersonal()
    {
        $data = $_POST;
        // Some validations

        // Save in db
        $user = new User();
        $user->setFirstName($data['firstname']);
        $user->setLastName($data['lastname']);
        $user->setTelephone($data['telephone']);
        $id = $user->findOrCreate();

        // Save in cookies
        $_SESSION['user_id'] = $id;
        $_SESSION['step'] = self::ADDRESS_STEP;

        header('Location: ../step2');
        exit;
    }

    public function storeAddress()
    {
        $data = $_POST;
        // Some validations

        // Step check
        if (!isset($_SESSION['step'])) {
            header('Location: step' . self::PERSONAL_STEP);
            exit;
        }
        if ($_SESSION['step'] > self::ADDRESS_STEP) {
            header('Location: step'  . self::BANK_STEP);
            exit;
        }

        // Save in db
        $address = new Address();
        $address->setUserId($_SESSION['user_id']);
        $address->setCity($data['city']);
        $address->setStreet($data['street']);
        $address->setHouseNumber($data['houseNumber']);
        $address->setZipCode($data['zipCode']);
        $address->findOrCreate();

        $_SESSION['step'] = self::BANK_STEP;

        header('Location: ../step2');
        exit;
    }

    public function storeBankAccount()
    {
        $data = $_POST;
        // Some validations

        // Step check
        if (!isset($_SESSION['step'])) {
            header('Location: step' . self::PERSONAL_STEP);
            exit;
        }

        // Save in db
        $bankAccount = new BankAccount();
        $bankAccount->setOwner($data['owner']);
        $bankAccount->setIban($data['iban']);
        $bankAccount->findOrCreate();
        $paymentId = $bankAccount->sendToApi();

        $_SESSION['step'] = self::BANK_STEP + 1;

        echo (new ViewHelper())->view('register/success', ['paymentId' => $paymentId]);
    }
}