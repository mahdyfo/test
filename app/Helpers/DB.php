<?php

namespace App\Helpers;

use Doctrine\DBAL\DriverManager;
use Exception;

class DB
{
    public static $instance;
    public static $connection;

    public function __construct()
    {

        $configs = [
            'dbname' => 'testdb1',
            'user' => 'root',
            'password' => '',
            'host' => '127.0.0.1',
            'driver' => 'pdo_mysql',
        ];

        try {
            self::$connection = DriverManager::getConnection($configs);
        } catch (Exception $e) {
            echo 'Error connecting to DB'; exit;
        }
    }

    /**
     * Get the singleton instance
     *
     * @return DB
     */
    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new DB;
        }

        return self::$instance;
    }

    /**
     * Init query builder every time it is needed to avoid conflicts but the connection will always remain the same
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function queryBuilder(){
        return self::$connection->createQueryBuilder();
    }
}
