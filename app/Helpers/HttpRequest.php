<?php

namespace App\Helpers;

class HttpRequest
{
    public function post($url, $body)
    {
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, $body);
        $result = curl_exec($c);

        if ($result) {
            $result = json_decode($result);

            if (!empty($result)) {
                return $result;
            }
        }

        return false;
    }
}