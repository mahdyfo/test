<?php

namespace App\Helpers;

class ViewHelper
{
    public function view($file, $variables = [])
    {
        extract($variables);

        // Get the view contents
        ob_start();
        require  'views/' . $file . '.php';
        $strView = ob_get_contents();
        ob_end_clean();

        return $strView;
    }
}