<?php

namespace App\Models;

class Address extends BaseModel
{
    protected $tableName = 'addresses';

    // Primary and Foreign keys
    private $id;
    protected $user_id;

    protected $city;
    protected $street;
    protected $houseNumber;
    protected $zipCode;

    // Setters

    protected function setId(int $id): void
    {
        $this->id = (int)$id;
    }

    public function setUserId(int $userId): void
    {
        $this->user_id = (int)$userId;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @param string $houseNumber To give the user freedom to choose multi numbers like 2-4 if house has multiple
     * @return void
     */
    public function setHouseNumber(string $houseNumber): void
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @param string $zipCode string To give the user freedom to include alphabets in zip code
     *        It can be validated using third party libraries
     * @return void
     */
    public function setZipCode($zipCode): void
    {
        $this->zipCode = $zipCode;
    }


    // Getters

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function getZipCode(): string
    {
        return $this->zipCode;
    }
}