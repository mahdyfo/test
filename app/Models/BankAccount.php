<?php

namespace App\Models;

use App\Helpers\HttpRequest;

class BankAccount extends BaseModel
{
    protected $tableName = 'bank_accounts';

    // Primary and Foreign keys
    private $id;
    protected $user_id;

    protected $owner;
    protected $iban;

    // Setters

    protected function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setOwner(string $owner): void
    {
        $this->owner = $owner;
    }

    public function setIban(string $iban): void
    {
        $this->iban = $iban;
    }


    // Getters

    public function getId(): int
    {
        return $this->id;
    }

    public function getOwner(): string
    {
        return $this->owner;
    }

    public function getIban(): string
    {
        return $this->iban;
    }

    public function sendToApi()
    {
        $request = new HttpRequest();

        // API (url should be in a config file, for more speed I place it here)
        $result = $request->post(
            'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data',
            json_encode([
                'customerId' => (int)$_SESSION['user_id'],
                'iban' => $this->getIban(),
                'owner' => $this->getOwner()
            ])
        );

        if (!empty($result->paymentDataId)) {
            return $result->paymentDataId;
        }

        return false;
    }
}