<?php
namespace App\Models;

use App\Helpers\DB;

class BaseModel {
    protected $tableName;
    protected $primary_key = 'id';

    /**
     * Find by primary key
     *
     * @param $id
     * @return mixed[]
     * @throws \Doctrine\DBAL\Exception
     */
    public function find($id) {
        $db = DB::getInstance()->queryBuilder();
        $results = $db->select('*')
            ->from($this->tableName)
            ->where($this->primary_key . ' = ?')
            ->setParameter(0, $id)
            ->execute()
            ->fetch();

        return $results;
    }

    /**
     * @return bool|int bool when it is saved, id of it when it already exists
     * @throws \Doctrine\DBAL\Exception
     */
    public function findOrCreate() {
        // Get the object properties to save
        $input_data = get_object_vars($this);

        // Remove default properties from it
        unset($input_data['tableName'], $input_data['primary_key']);

        // Prepare insert data for binding
        $columns = [];
        $values = [];
        foreach($input_data as $key => $value){
            $columns[$key] = '?';
            $values[]  = $value;
        }

        $db = DB::getInstance()->queryBuilder();
        try{
            $db = $db->insert($this->tableName)->values($columns);

            foreach($values as $key => $value){
                $db = $db->setParameter($key, $value);
            }

            $db->execute();

            if (method_exists($this, 'setId')) {
                $this->setId($db->getConnection()->lastInsertId());
            }

            return true;
        }catch(\Exception $e){
            // Ignore duplications, return id if exists

            $db = DB::getInstance()->queryBuilder();
            $query = $db->select('id')
                ->from($this->tableName);

            $i = 0;
            foreach($input_data as $key => $value){
                $query = $query->andWhere($key . ' = ?');
                $query = $query->setParameter($i, $value);
                $i++;
            }

            $result = $query->execute()->fetchOne();

            if ($result) {
                return $result;
            }
            return false;
        }
    }
}