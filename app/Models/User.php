<?php

namespace App\Models;

class User extends BaseModel
{
    protected $tableName = 'users';

    private $id = 0;
    protected $firstName;
    protected $lastName;
    protected $telephone;

    // Setters

    protected function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    public function setTelephone($telephone): void
    {
        $this->telephone = $telephone;
    }

    // Getters

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string to give the user freedom to include dash in phone numbers
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }
}