<?php
require_once 'vendor/autoload.php';
require_once 'routes.php';

session_start();

$url = $_SERVER['REQUEST_URI'];

// Remove first url segment
$url = explode('/', $url);
unset($url[0], $url[1]);
$url = implode('/', $url);

// Call controller method that we got from routes
$controller = [];
if (isset($routes[$url])) {
    $controller = $routes[$url];
    $controller[0] = new $controller[0];

    echo call_user_func($controller);
}

exit;