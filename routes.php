<?php

use App\Controllers\RegisterController;

$routes = [
    ''                => [RegisterController::class, 'showLastSaved'],
    'register/step1' => [RegisterController::class, 'showPersonalForm'],
    'register/step2' => [RegisterController::class, 'showAddressForm'],
    'register/step3' => [RegisterController::class, 'showPaymentForm'],

    'register/step1/store' => [RegisterController::class, 'storePersonal'],
    'register/step2/store' => [RegisterController::class, 'storeAddress'],
    'register/step3/store' => [RegisterController::class, 'storeBankAccount'],
];
